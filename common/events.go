package common

import (
	"context"
	"log"

	kafka "github.com/segmentio/kafka-go"
)

// EventHandler handles communication with the event backend, Kafka
type EventHandler struct {
	config  map[string]string
	writers map[string]*kafka.Writer
}

// TopicCallback is a function that is called whenever the subscribed topic receives a message.
type TopicCallback func(key string, value string)

type reader struct {
	reader   *kafka.Reader
	topic    string
	offset   int
	callback TopicCallback
	ctx      context.Context
}

// CreateEventHandler instantiates the event handler
func CreateEventHandler(config map[string]string) EventHandler {
	h := EventHandler{}
	h.config = config
	h.writers = make(map[string]*kafka.Writer)
	h.Write(context.Background(), "service-registration", config["Address"], "true")
	return h
}

// Subscribe to an event-topic.
func (h *EventHandler) Subscribe(ctx context.Context, topic string, callback TopicCallback, readOffset int) {
	r := reader{
		reader: kafka.NewReader(kafka.ReaderConfig{
			Brokers: []string{h.config["BrokerIP"]},
			Topic:   topic}),
		topic:    topic,
		offset:   readOffset,
		callback: callback,
		ctx:      ctx,
	}
	go r.readLoop()
}

// Write a message to a topic.
func (h *EventHandler) Write(ctx context.Context, topic string, key string, value string) {
	var writer *kafka.Writer
	if w, ok := h.writers[topic]; ok {
		writer = w
	} else {
		writer = kafka.NewWriter(kafka.WriterConfig{
			Brokers: []string{h.config["BrokerIP"]},
			Topic:   topic,
		})
		h.writers[topic] = writer
	}
	writer.WriteMessages(ctx, kafka.Message{
		Key:   []byte(key),
		Value: []byte(value),
	})
}

func (r *reader) readLoop() {
	for {
		m, err := r.reader.ReadMessage(r.ctx)
		if err != nil {
			log.Println("Error reading message in topic ", r.topic, " : ", err)
			continue
		}
		r.callback(string(m.Key), string(m.Value))
	}
}
