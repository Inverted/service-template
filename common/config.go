package common

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

// ServiceDescription describes which methods the service supports, and to which kafka topics it is subscribed.
type ServiceDescription struct {
	GET           []string `json:"GET"`
	POST          []string `json:"POST"`
	Topics        []string `json:"topics"`
	ConsumerGroup string   `json:"consume-group"`
	ProducerGroup string   `json:"producer-group"`
}

// LoadConfig loads the config, expecting all strings to have a value.
// If DEV_ENV=false is set, it tries config_prod.json, otherwise config_dev.json.
// It also supports reading environment variables (all-caps) instead.
func LoadConfig(keys ...string) map[string]string {
	keys = append(keys, "Address")
	keys = append(keys, "BrokerIP")
	// Make map and set keys to empty values
	var config = make(map[string]string)
	for _, key := range keys {
		config[key] = ""
	}
	file := "config_dev.json"
	if os.Getenv("DEV_ENV") == "false" {
		file = "config_prod.json"
	}

	checkErr(tryFile(config, file))
	tryEnvironment(config, keys)

	checkErr(checkValidConfig(config, keys))
	return config
}

func tryFile(config map[string]string, fileName string) error {
	dat, err := ioutil.ReadFile(fileName)
	if err == nil {
		target := make(map[string]string)
		err = json.Unmarshal(dat, &target)
		unknown := make([]string, 0)
		for tKey, tVal := range target {
			if _, ok := config[tKey]; ok {
				config[tKey] = tVal
			} else {
				unknown = append(unknown, tKey)
			}
		}
		if len(unknown) != 0 {
			return fmt.Errorf("Unkown key(s) %v in config file", unknown)
		}
	}
	return nil
}

func tryEnvironment(config map[string]string, keys []string) {
	for _, key := range keys {
		if val := os.Getenv(strings.ToUpper(key)); val != "" {
			config[key] = val
		}
	}
}

func checkValidConfig(config map[string]string, keys []string) error {
	missing := make([]string, 0)
	for key, val := range config {
		if val == "" {
			missing = append(missing, key)
		}
	}
	if len(missing) != 0 {
		return fmt.Errorf("Missing config values for %v", missing)
	}
	return nil
}

func checkErr(err error) {
	if err != nil {
		log.Println(err)
		os.Exit(-1)
	}
}
