package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"gitlab.com/toolassist/service-template/common"
)

type httpHandler struct {
	description []byte
	sd          common.ServiceDescription
	events      common.EventHandler
}

func createHandler(config map[string]string) *httpHandler {
	h := &httpHandler{}
	desc, err := ioutil.ReadFile("service.json")
	if err != nil {
		log.Println("Could not find service.json")
		os.Exit(-1)
	}
	err = json.Unmarshal(desc, &h.sd)
	if err != nil {
		log.Println("service.json contains invalid JSON")
		os.Exit(-1)
	}
	h.description = desc
	h.events = common.CreateEventHandler(config)
	return h
}

func (h *httpHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.RequestURI {
	case "/":
		w.Write(h.description)
	case "/hello":
		w.Write([]byte("Hello world!"))
	}
}

func main() {
	config := common.LoadConfig()
	httpHandler := createHandler(config)
	s := &http.Server{
		Addr:    config["Address"],
		Handler: httpHandler,
	}
	s.ListenAndServe()
}
