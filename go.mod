module gitlab.com/toolassist/service-template

go 1.14

require (
	github.com/segmentio/kafka-go v0.4.6
	github.com/stretchr/testify v1.6.1 // indirect
	golang.org/x/net v0.0.0-20201016165138-7b1cca2348c0 // indirect
)
